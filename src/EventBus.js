import Vue from 'vue'

export const EventBus = new Vue()

export const SHOW_LOADING = 'SHOW_LOADING'
export const HIDE_LOADING = 'HIDE_LOADING'
export const SHOW_SUCCESS = 'SHOW_SUCCESS'
export const SHOW_INFO = 'SHOW_INFO'
export const SHOW_ERROR = 'SHOW_ERROR'
export const SHOW_WARNING = 'SHOW_WARNING'
export const CONFIRM_INFO = 'CONFIRM_INFO'
export const CONFIRM_SUCCESS = 'CONFIRM_SUCCESS'
export const CONFIRM_ERROR = 'CONFIRM_ERROR'
export const CONFIRM_WARNING = 'CONFIRM_WARNING'
export const FLASH_INFO = 'FLASH_INFO'
export const FLASH_SUCCESS = 'FLASH_SUCCESS'
export const FLASH_ERROR = 'FLASH_ERROR'
export const FLASH_WARNING = 'FLASH_WARNING'
export const CHANGE_LOCALE = 'CHANGE_LOCALE'
export const LOCALE_CHANGED = 'LOCALE_CHANGED'
export const UNKNOWN_ERROR = 'An unknown error has occurred'
export const DATE_FORMAT = 'YYYY-MM-DD'

const EventBusLoadingIDs = []

export function ClearLoading() {
  EventBus.$emit(HIDE_LOADING)
  EventBusLoadingIDs.splice(0, EventBusLoadingIDs.length)
}

export function ShowLoading(id) {
  if (EventBusLoadingIDs.indexOf(id) <= -1) {
    EventBusLoadingIDs.push(id)
  }
  EventBus.$emit(SHOW_LOADING)
  // console.log(`Event Bus Show Loading = ${EventBusLoadingIDs}`)
}

export function HideLoading(id) {
  if (EventBusLoadingIDs.indexOf(id) > -1) {
    EventBusLoadingIDs.splice(EventBusLoadingIDs.indexOf(id), 1)
  }
  // console.log(`Event Bus Hide = ${EventBusLoadingIDs}, length = ${EventBusLoadingIDs.length}`)
  if (EventBusLoadingIDs.length === 0) {
    // console.log('Hide Loader')
    EventBus.$emit(HIDE_LOADING)
  }
}

// export function ShowSuccess (id) {
//   if (EventBusLoadingIDs.indexOf(id) > -1) {
//     EventBusLoadingIDs.splice(EventBusLoadingIDs.indexOf(id), 1)
//   }
//   // console.log(`Event Bus Hide = ${EventBusLoadingIDs}, length = ${EventBusLoadingIDs.length}`)
//   if (EventBusLoadingIDs.length === 0) {
//     // console.log('Hide Loader')S
//     EventBus.$emit(SHOW_SUCCESS)
//   }
// }
