export function dynamicColor() {
  var r = Math.floor(Math.random() * 255)
  var g = Math.floor(Math.random() * 255)
  var b = Math.floor(Math.random() * 255)
  return 'rgb(' + r + ',' + g + ',' + b + ')'
}

export const defaultColors = [
  'blue',
  'red',
  'green',
  'yellow',
  '#EB5A2A',
  'orange',
  '#FC05DB',
  'gray',
  'purple',
  'cyan',
  '#05FC26',
  'maroon',
  'olive',
  'navy',
  '#2ABBEB'
]
