import bowser from 'bowser'
// import store from './store'

const LE = require('@/assets/le.min')
LE.init({ token: 'c369f455-ecd9-4c13-a1c4-cb86868074ec', catchall: true, trace: true, print: true })

export function LogIt(event, object) {
  LE.log({
    Event: event,
    LogData: object,
    Browser: bowser.name + ' ' + bowser.version
  })
}

export function LogWarning(event, object) {
  LE.warn({
    Event: event,
    LogData: object,
    Browser: bowser.name + ' ' + bowser.version
  })
}

export function LogError(event, object) {
  LE.error({
    Event: event,
    LogData: object,
    Browser: bowser.name + ' ' + bowser.version
  })
}

export function LogInfo(event, object) {
  LE.info({
    Event: event,
    LogData: object,
    Browser: bowser.name + ' ' + bowser.version
  })
}
