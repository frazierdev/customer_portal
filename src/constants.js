
let baseUrl = ''
let debug = true

switch (process.env.VUE_APP_MODE) {
  case 'production':
    baseUrl = 'https://customers.frazier.com/api/'
    debug = false
    break
  case 'development':
    baseUrl = 'http://localhost:62738/api/'
    break
  case 'beta':
    baseUrl = 'https://customers-beta.frazier.com/api/'
    break
  case 'test':
    baseUrl = 'https://customersdev.frazier.com/api/'
    break
  default:
    break
}

export const BaseUrl = baseUrl

export const DEBUG = debug
