// import { LogIt, LogWarning } from './logentries'
// import { onNull, nullIf, formatDate } from '@/functions'
import { ShowLoading, HideLoading /* , EventBus, SHOW_INFO, SHOW_SUCCESS, SHOW_ERROR, SHOW_WARNING, CONFIRM_INFO, CONFIRM_SUCCESS, CONFIRM_ERROR, CONFIRM_WARNING */ } from '@/EventBus'
import Vue from 'vue'
import { onNull, ConfirmAlert, Alert } from '@/functions'
// import swal from 'sweetalert'

// EventBus.$on(SHOW_INFO, (msg) => { Alert(msg, 'info') })
// EventBus.$on(SHOW_SUCCESS, (msg) => { Alert(msg, 'success') })
// EventBus.$on(SHOW_ERROR, (msg) => { Alert(msg, 'error') })
// EventBus.$on(SHOW_WARNING, (msg) => { Alert(msg, 'warning') })
// EventBus.$on(CONFIRM_INFO, (msg, onSuccessFx = () => { }, onCancelFx = () => { }) => { ConfirmAlert(msg, 'info', onSuccessFx, onCancelFx) })
// EventBus.$on(CONFIRM_SUCCESS, (msg, onSuccessFx = () => { }, onCancelFx = () => { }) => { ConfirmAlert(msg, 'success', onSuccessFx, onCancelFx) })
// EventBus.$on(CONFIRM_ERROR, (msg, onSuccessFx = () => { }, onCancelFx = () => { }) => { ConfirmAlert(msg, 'error', onSuccessFx, onCancelFx) })
// EventBus.$on(CONFIRM_WARNING, (msg, onSuccessFx = () => { }, onCancelFx = () => { }) => { ConfirmAlert(msg, 'warning', onSuccessFx, onCancelFx) })

Vue.mixin({
  methods: {
    //   LogIt,
    //   LogWarning,
    onNull,
    //   nullIf,
    //   formatDate,
    ShowLoading,
    HideLoading,
    ConfirmAlert,
    Alert
  }
})
