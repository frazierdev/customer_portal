export const USER_LOCALE = 'USER_LOCALE'
export const USER_ID = 'USER_ID'
export const USER_TOKEN = 'USER_TOKEN'
export const USER_ROLE = 'USER_ROLE'
export const USER_PASS = 'USER_PASS'
export const USER_PROXY = 'USER_PROXY'
export const FULL_ACCESS = 'FULL_ACCESS'
export const GALLERY_TOKEN = 'GALLERY_TOKEN'
// export const PROXY_TOKEN = 'PROXY_TOKEN'

export function SetSessionStorage(name, value) {
  sessionStorage.setItem(name, value)

  if (process.env.NODE_ENV !== 'production') {
    console.log(`Set ${name}: ${value}`)
  }
}

export function RemoveSessionStorage(name) {
  sessionStorage.removeItem(name)
}

export function GetSessionStorage(name, _default) {
  if (sessionStorage.getItem(name)) {
    var x = sessionStorage.getItem(name)
    if (x === 'true' || x === 'false') {
      return x === 'true'
    } else if (!isNaN(x)) {
      return Number(x)
    } else if (x == 'null') {
      return null
    } else {
      return x
    }
  } else {
    return _default === undefined ? null : _default
  }
}

export function SetLocalStorage(name, value) {
  localStorage.setItem(name, value)
}

export function GetLocalStorage(name, _default, type) {
  if (localStorage.getItem(name)) {
    if (type === 'bool') {
      return localStorage.getItem(name) === 'true'
    } else {
      return localStorage.getItem(name)
    }
  } else {
    return _default === undefined ? null : _default
  }
}
