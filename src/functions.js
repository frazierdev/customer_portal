import { ShowLoading, HideLoading } from './EventBus'
import axios from 'axios'
import moment from 'moment'
// import swal from 'sweetalert'
import Vue from 'vue'

export function onNull(val, _default) {
  if (val === undefined || val === null) {
    return _default
  }
  return val
}

export function randomString(len, charSet) {
  charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
  len = len || 20
  var randomString = ''
  for (var i = 0; i < len; i++) {
    var randomPoz = Math.floor(Math.random() * charSet.length)
    randomString += charSet.substring(randomPoz, randomPoz + 1)
  }
  return randomString
}

export const rules = {
  email: [
    v => !!v || 'E-mail is required',
    v => /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/.test(v) || 'E-mail must be valid'
  ],
  required: [
    v => !!v || 'Value is required'
  ],
  phone: [
    v => /^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$/.test(v) || 'Invalid phone number; try any following format: XXX-XXX-XXXX OR (XXX) XXX-XXXX OR XXX XXX XXXX'
  ],
  date: [
    v => !!v || 'Date is required',
    // v => moment(v).isValid() || 'Date must be valid'
    v => /^\d{1,2}\/\d{1,2}\/(\d{2}|\d{4})$/.test(v) || 'Date must be valid'
    // v => /^\d{1,2}\/\d{1,2}\/\d{4}$/.test(v) || 'Date must be valid'
  ],
  password: [
    v => !!v || 'Password is required',
    v => v.length >= 12 || 'Password requires 12 characters minimum',
    v => /[!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?]+/.test(v) || 'Must have at least one special character'
  ]
}

export function DownloadFile(url, filename) {
  ShowLoading('Download File')
  axios({
    url: url,
    method: 'GET',
    responseType: 'blob'
  }).then((response) => {
    HideLoading('Download File')
    let url = window.URL.createObjectURL(new Blob([response.data]))
    let link = document.createElement('a')
    link.href = url
    link.setAttribute('download', filename)
    document.body.appendChild(link)
    link.click()
    document.body.removeChild(link)
    url = null
    link = null
  })
}

export function formatDate(date, format = 'LL') {
  return moment(date).format(format)
}

export function readableBytes(bytes) {
  var i = Math.floor(Math.log(bytes) / Math.log(1024))
  var sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']

  return (bytes / Math.pow(1024, i)).toFixed(2) * 1 + ' ' + sizes[i]
}

export function backgroundStyle() {
  return {
    backgroundImage: 'url(img/rack/high-rise.jpg)',
    backgroundSize: 'cover',
    backgroundAttachment: 'fixed',
    height: 'calc(100vh - 130px)'
  }
}

export function filtered(items, filter) {
  var self = this
  var records

  if (onNull(filter, '').length > 0) {
    // function GetValue (item, field) {
    //   if (self.valueConversionFunction != null) {
    //     return self.valueConversionFunction(item[field], field, item)
    //   } else {
    //     return item[field]
    //   }
    // }

    var data = items.filter(function(obj) {
      return Object.keys(obj).some(function(e) {
        // if (!self.fields.some(p => p == e)) return false
        return onNull(obj[e], '').toString().toLowerCase().contains(filter.toLowerCase())
      })
    })

    records = data
  } else {
    records = items
  }
  return records
}


export function ConfirmAlert(msg, type, onSuccessFx, onCancelFx) {
  let wrapper = document.createElement('div')
  wrapper.style.textAlign = 'center'
  wrapper.style.width = '100%'
  wrapper.style.fontFamily = 'Roboto'
  wrapper.innerHTML = '<br>' + msg

  Vue.$swal({
    // text: msg,
    content: wrapper,
    icon: type,
    dangerMode: type == 'warning',
    buttons: {
      cancel: 'Cancel',
      confirm: 'OK'
    }
  }).then(answer => {
    setTimeout(function() { wrapper.remove() }, 1000)
    // console.log(answer)
    if (answer) {
      onSuccessFx()
    } else {
      if (onCancelFx != undefined) {
        onCancelFx()
      }
    }
  })
}

export function Alert(msg, icon) {
  let wrapper = document.createElement('div')
  wrapper.style.textAlign = 'center'
  wrapper.style.width = '100%'
  wrapper.style.fontFamily = 'Roboto'
  wrapper.style.wordBreak = 'break-word'
  wrapper.innerHTML = '<br>' + msg
  Vue.$swal({ icon, content: wrapper }).then(() => {
    setTimeout(function() { wrapper.remove() }, 1000)
  })
}
