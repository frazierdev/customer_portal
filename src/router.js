import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store'
import Login from '@/views/Login'

Vue.use(Router)

const router = new Router({
  // mode: 'hash', // Demo is living in GitHub.io, so required!
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  // scrollBehavior: (to, from, savedPosition) => {
  //   if (to.hash) {
  //     return {
  //       selector: to.hash,
  //       behavior: 'smooth',
  //     }
  //   }
  // },
  routes: [
    {
      path: '/',
      redirect: '/home',
      name: 'Base',
      component: () => import('@/views/BaseWrapper'),
      // props: true,
      children: [
        {
          path: '/home',
          name: 'Home',
          component: () => import('@/views/Home')
        },
        {
          path: '/surveys',
          name: 'SurveyList',
          component: () => import('@/views/SurveyList')
        },
        {
          path: '/survey/:orderNum',
          // path: '/survey/:orderNum(\\d+)',
          name: 'SurveyForm',
          component: () => import('@/views/SurveyForm'),
          props: true
          // props: {
          //   orderNum: Number
          // }
        },
        {
          path: '/surveyEditor',
          name: 'SurveyEditor',
          component: () => import('@/views/SurveyEditor')
        },
        {
          path: '/surveyResponses',
          name: 'SurveyResponses',
          component: () => import('@/views/SurveyResponses')
        },
        {
          path: '/reports',
          name: 'Reports',
          component: () => import('@/views/Reports')
        },
        {
          path: '/contacts',
          name: 'Contacts',
          component: () => import('@/views/Contacts'),
          props: true
        },
        {
          path: '/orders',
          name: 'Orders',
          component: () => import('@/views/Orders')
        },
        {
          path: '/order/:orderNum',
          name: 'Order',
          component: () => import('@/views/Order'),
          props: true
        },
        // {
        //   path: '/billing',
        //   name: 'Billing',
        //   component: () => import('@/views/Billing'),
        //   props: true
        // },
        {
          path: '/homeMaintenance',
          name: 'HomeMaintenance',
          component: () => import('@/views/HomeMaintenance')
          // meta: { adminAuth: true }
        },
        {
          path: '/homeContent/:id',
          name: 'HomeContent',
          component: () => import('@/views/HomeContent'),
          props: true
          // meta: { adminAuth: true }
        },
        {
          path: '/admin',
          name: 'Admin',
          component: () => import('@/views/Admin')
          // meta: { adminAuth: true }
        },
        {
          path: '/master',
          name: 'Master',
          component: () => import('@/views/Master')
          // meta: { adminAuth: true }
        },
        {
          path: '/newAccount',
          name: 'NewAccount',
          component: () => import('@/views/NewAccount')
        },
        {
          path: '/addAccount',
          name: 'AddAccount',
          component: () => import('@/views/AddAccount')
        },
        {
          path: '/changePassword',
          name: 'ChangePassword',
          component: () => import('@/views/ChangePassword')
        },
        {
          path: '/profile',
          name: 'Profile',
          component: () => import('@/views/Profile')
        },
        {
          path: '/search',
          name: 'Search',
          component: () => import('@/views/Search'),
          props: true
        },
        {
          path: '/photos',
          name: 'Photos',
          component: () => import('@/views/Photos')
        },
      ]
    },
    {
      path: '/pages',
      redirect: '/pages/404',
      name: 'Pages',
      component: {
        render(c) { return c('router-view') }
      },
      children: [
        {
          path: 'login',
          name: 'Login',
          component: Login
        },
        {
          path: 'forgotPassword',
          name: 'ForgotPassword',
          component: () => import('@/views/ForgotPassword')
        },
        {
          path: 'photogallery',
          name: 'PhotoGallery',
          component: () => import('@/views/PhotoGallery')
        },
        {
          path: 'resetPassword/:resetID',
          name: 'ResetPassword',
          component: () => import('@/views/ResetPassword'),
          props: true
        }
      ]
    }
  ]
})

router.beforeEach((to, from, next) => {
  // console.log(`hasToken: ${store.state.user.token !== null}`)
  // console.log(`from: ${from.name}`)
  // console.log(`to: ${to.name}`)
  // LogIt('Change Route', { Route: to.name })
  // if (store.state.user.token === null && to.name !== 'Login' && to.name !== 'NewAccount' && to.name !== 'ForgotPassword' && to.name !== 'ResetPassword') {
  let bypass = ['Login', 'ForgotPassword', 'ResetPassword']
  let allowed = ['PhotoGallery']

  try {
    if (allowed.includes(to.name)) {
      next()
    } else if (store.state.user.token === null && !bypass.includes(to.name)) {
    // if (store.state.user.token === null && to.name  !== 'Login' && to.name !== 'ForgotPassword' && to.name !== 'ResetPassword') {
      console.log(`not logged in so re-route ${from.name} to Login page`)
      next({ name: 'Login' })
    } else if (store.state.user.token !== null && bypass.includes(to.name)) {
    // } else if (store.state.user.token !== null && (to.name === 'Login' || to.name === 'ForgotPassword' || to.name === 'ResetPassword')) {
      // } else if (store.state.user.token !== null && (to.name === 'Login' || to.name === 'NewAccount' || to.name === 'ForgotPassword' || to.name === 'ResetPassword')) {
      console.log('logged in so route to home')
      next({ name: 'Home' })
    } else {
      if ((store.state.user.newPass ?? false) === true && to.name !== 'ChangePassword' && !bypass.includes(to.name)) {
        // console.log(`store.state.user.newPass: ${store.state.user.newPass}`)
        console.warn('Password change is required')
        next({ name: 'ChangePassword' })
      } else {
        // Handle security so user can't directly navigate via route
        switch (to.name) {
          // case 'Workcenters':
          //   if (!store.state.user.security.editWorkcenters) {
          //     next({ name: 'Home' })
          //   } else {
          //     next()
          //   }
          //   break
          default:
            console.warn(`Default Router Security - ${to.name}`)
            next()
        }
      }
    }
  } catch (e) {
    console.error(e)
  }
})

// router.afterEach((to) => {
//   gtag('config', window.GA_TRACKING_ID, {
//     page_path: to.fullPath,
//     app_name: 'Frazier Customer Portal',
//     send_page_view: true
//   })
// })

export default router

// export default new Router({
//   // mode: 'history',
//   base: process.env.BASE_URL,
//   routes: [
//     {
//       path: '/',
//       name: 'home',
//       component: Home
//     }
//   ]
// })
