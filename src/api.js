'use strict'

import Vue from 'vue'
import axios from 'axios'
import { ClearLoading, EventBus, SHOW_ERROR } from '@/EventBus'
import { Alert } from '@/functions'
import store from '@/store'
import { LogError } from '@/logentries'
import { BaseAPIUrl } from '@/plugins/axios'
import { DEBUG } from '@/constants'

let _config = {
  // baseURL: process.env.baseURL || process.env.apiUrl || ""
  baseURL: BaseAPIUrl
  // timeout: 60 * 1000, // Timeout
  // withCredentials: true, // Check cross-site Access-Control
}

const _axios = axios.create(_config)

// Full config:  https://github.com/axios/axios#request-config
// axios.defaults.baseURL = process.env.baseURL || process.env.apiUrl || ''
_axios.defaults.baseURL = BaseAPIUrl
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN
_axios.defaults.headers.common['Authorization'] = 'Bearer ' + store.state.user.token
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'


_axios.interceptors.request.use(
  function(config) {
    try {
      // if (config.url.indexOf('/') > -1) {
      config.baseURL = BaseAPIUrl
      // } else {
      //   config.baseURL = BaseAPIUrl + 'web/'
      // }

      if (process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'training') {
        console.log(`axios api ${config.method} url = ${config.baseURL}${config.url}`)
        if (config.params !== undefined && config.params !== null) {
          console.log(`axios api parameters = ${JSON.stringify(config.params)}`)
        }
        if (config.data !== undefined && config.data !== null) {
          console.log(`axios api data = ${JSON.stringify(config.data)}`)
        }
      }

      config.headers.common['Authorization'] = 'Bearer ' + store.state.user.token
      if (config.method === 'get') {
        if (config.params === undefined) {
          config.params = {}
        }
      } else {
        if (config.data === undefined || config.data === null) {
          config.data = {}
        }
      }
    } catch (e) {
      LogError(e)
    }

    return config
  },
  function(error) {
    ClearLoading()

    if (DEBUG) {
      if (error) {
        let msg = error.response.data || error.response
        Alert(msg, 'error')
      }
    }
    LogError(error)
    return Promise.reject(error)
  })

_axios.interceptors.response.use(function(response) {
  if (process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'training') {
    console.log(response.data)
  }
  return response
}, function(error) {
  ClearLoading()
  LogError(error)
  // if (error) Alert(error.response.data, 'error')
  if (error) {
    let msg = error.response.data || error.response
    Alert(msg, 'error')
  }
  return Promise.reject(error)
})

const api = {
  baseAPIUrl: BaseAPIUrl,
  get(url, data = {}, config = {}) {
    config.params = data
    if (DEBUG) console.info(config)
    return _axios.get(url, config)
  },
  post(url, data = {}) {
    if (DEBUG) console.info(data)
    return _axios.post(url, data)
  },
  all(requestsArray) { return axios.all(requestsArray) },
  spread(spreadFunc) { return axios.spread(spreadFunc) }
}

export default api

// Plugin.install = function(Vue) {
//   Vue.axios = _axios
//   window.axios = _axios
//   Object.defineProperties(Vue.prototype, {
//     axios: {
//       get() {
//         return _axios
//       }
//     },
//     $axios: {
//       get() {
//         return _axios
//       }
//     }
//   })
// }

// Vue.use(Plugin)

// export default Plugin
