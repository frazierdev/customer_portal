'use strict'

import Vue from 'vue'
import axios from 'axios'
import { ClearLoading } from '@/EventBus'
import store from '@/store'
import { LogError } from '@/logentries'
import { BaseUrl } from '@/constants'

export const BaseAPIUrl = BaseUrl
// Full config:  https://github.com/axios/axios#request-config
// axios.defaults.baseURL = process.env.baseURL || process.env.apiUrl || ''
axios.defaults.baseURL = BaseAPIUrl
// axios.defaults.headers.common['Authorization'] = AUTH_TOKEN
axios.defaults.headers.common['Authorization'] = 'Bearer ' + store.state.user.token
// axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'

// let _config = {
//   // baseURL: process.env.baseURL || process.env.apiUrl || ""
//   baseURL: BaseAPIUrl
//   // timeout: 60 * 1000, // Timeout
//   // withCredentials: true, // Check cross-site Access-Control
// }

// const _axios = axios.create(_config)

axios.interceptors.request.use(
  function(config) {
    try {
      // if (config.url.indexOf('/') > -1) {
      config.baseURL = BaseAPIUrl
      // } else {
      //   config.baseURL = BaseAPIUrl + 'web/'
      // }

      if (process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'training') {
        console.log(`axios ${config.method} url = ${config.baseURL}${config.url}`)
        if (config.params !== undefined && config.params !== null) {
          console.log(`axios parameters = ${JSON.stringify(config.params)}`)
        }
        if (config.data !== undefined && config.data !== null) {
          console.log(`axios data = ${JSON.stringify(config.data)}`)
        }
      }

      config.headers.common['Authorization'] = 'Bearer ' + store.state.user.token
      if (config.method === 'get') {
        if (config.params === undefined) {
          config.params = {}
        }
      } else {
        if (config.data === undefined || config.data === null) {
          config.data = {}
        }
      }
    } catch (e) {
      LogError(e)
    }

    return config
  },
  function(error) {
    ClearLoading()
    LogError(error)
    return Promise.reject(error)
  })

axios.interceptors.response.use(function(response) {
  if (process.env.NODE_ENV !== 'production' && process.env.NODE_ENV !== 'training') {
    console.log(response.data)
  }
  return response
}, function(error) {
  ClearLoading()
  LogError(error)
  return Promise.reject(error)
})


// Plugin.install = function(Vue) {
//   Vue.axios = axios
//   window.axios = axios
//   Object.defineProperties(Vue.prototype, {
//     axios: {
//       get() {
//         return axios
//       }
//     },
//     $axios: {
//       get() {
//         return axios
//       }
//     }
//   })
// }

// Vue.use(Plugin)

// export default Plugin
