// import '@font'
import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import 'vuetify/dist/vuetify.min.css'
import RackmanFilled from '@/components/RackmanFilled.vue'
import RackmanOutline from '@/components/Rackman.vue'

Vue.use(Vuetify)

export default new Vuetify({
  icons: {
    iconfont: 'fa',
    values: {
      // rackman: RackmanSvg
      rackman_filled: {
        component: RackmanFilled
        // component: require('@/assets/icons/test_svg.svg')
        // component: require('@/assets/rackman.svg')
        // component: require('~/assets/logo.svg')
      },
      rackman_outline: {
        component: RackmanOutline
      }
      // rackman: RackmanSvg,
      // rackmanPath: 'M1260 2579 c-410 -104 -751 -189 -757 -189 -10 0 -13 -200 -13 -964 0 -934 1 -966 20 -1010 48 -113 146 -174 295 -183 78 -5 97 -1 864 193 606 153 790 203 810 220 l26 22 3 892 c1 585 -1 905 -8 930 -13 48 -50 66 -106 51 -22 -5 -50 -12 -64 -15 -14 -3 -344 -86 -735 -186 -390 -99 -711 -180 -713 -180 -2 0 -3 -422 -2 -937 1 -516 2 -948 1 -959 -1 -20 -5 -21 -38 -14 -21 5 -67 13 -102 19 -88 14 -160 63 -192 128 l-24 48 -5 935 c-3 514 -9 944 -13 955 -5 11 0 8 9 -7 11 -17 25 -26 34 -23 16 5 597 152 1158 294 l343 87 34 -23 c19 -13 34 -29 32 -35 -2 -13 115 -82 158 -92 28 -7 29 -6 10 7 -11 8 -53 33 -92 56 -71 41 -73 43 -73 82 0 52 -35 89 -82 88 -18 0 -368 -86 -778 -190z m806 144 c17 -17 15 -18 -34 -24 -29 -4 -60 -10 -69 -14 -20 -8 -484 -124 -578 -145 -33 -7 -64 -17 -70 -20 -5 -4 -23 -10 -40 -13 -16 -3 -156 -37 -310 -77 -386 -98 -389 -99 -408 -96 -60 9 0 43 108 62 17 2 44 9 60 14 33 10 182 50 205 55 8 2 125 31 260 65 547 139 826 208 842 209 9 1 24 -7 34 -16z m392 -225 c9 -9 12 -226 12 -913 0 -1028 11 -923 -95 -949 -69 -16 -1064 -268 -1295 -327 -85 -22 -160 -37 -167 -35 -11 4 -13 186 -13 939 l0 934 108 25 c252 61 719 180 737 187 23 10 504 125 580 139 74 14 119 14 133 0z'
    }
  }
})
