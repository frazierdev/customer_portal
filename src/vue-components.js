import upperFirst from 'lodash/upperFirst'
import camelCase from 'lodash/camelCase'
import Vue from 'vue'

const requireComponent = require.context(
  // The relative path of the components folder
  './components',
  // Whether or not to look in subfolders
  false,
  // The regular expression used to match base component filenames
  /_base-[A-Z]\w+\.(vue|js)$/
)
// console.log(`requireComponent.keys = ${requireComponent.keys()}`)
requireComponent.keys().forEach(fileName => {
  // console.log(fileName)
  // Get component config
  const componentConfig = requireComponent(fileName)

  // Get PascalCase name of component
  const componentName = upperFirst(
    camelCase(
      // Strip the leading `'./` and extension from the filename
      fileName.replace(/^\.\/(.*)\.\w+$/, '$1').replace('_base-', '')
    )
  )
  console.log(`register ${componentName}`)
  // Register component globally
  Vue.component(
    componentName,
    // Look for the component options on `.default`, which will
    // exist if the component was exported with `export default`,
    // otherwise fall back to module's root.
    componentConfig.default || componentConfig
  )
})

// Vue.component('scheduler', {
//   props: ['id', 'config'],
//   template: '<div :id="id"></div>',
//   mounted: function () {
//       this.control = new DayPilot.Scheduler(this.id, this.config).init() // eslint-disable-line
//   }
// })



