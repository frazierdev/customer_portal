/* eslint-disable */
String.prototype.format = function () {
  var args = arguments;
  return this.replace(/{(\d+)}/g, function (match, number) {
      return typeof args[number] != 'undefined'
          ? args[number]
          : match
          ;
  });
};

Number.prototype.USD = function (decimals) {
  if (decimals === undefined || decimals === null) { decimals = 2 }
  var val = this
  if (val === undefined || val === null) { val = 0 }
  return val.toLocaleString('en-US', { style: 'currency', currency: 'USD', maximumFractionDigits: decimals, minimumFractionDigits: decimals })
}

Number.prototype.NumberFormat = function (minDecimals, maxDecimals) {
  if (minDecimals === undefined || minDecimals === null) { minDecimals = 0 }
  if (maxDecimals === undefined || maxDecimals === null) { maxDecimals = minDecimals }
  var val = this || 0
  return val.toLocaleString('en-US', { style: 'decimal', currency: 'USD', maximumFractionDigits: maxDecimals, minimumFractionDigits: minDecimals })
}

Number.prototype.ToPercentage = function (decimals) {
  if (decimals === undefined || decimals === null) { decimals = 2 }
  var val = this
  if (val === undefined || val === null) { val = 0 }
  return val.toLocaleString('en-US', { style: 'percent', currency: 'USD', maximumFractionDigits: decimals, minimumFractionDigits: decimals })
}

Array.prototype.unique = function() {
  return this.filter(function (value, index, self) {
    return self.indexOf(value) === index;
  });
};

Array.prototype.distinct = function (fieldname) {
  if (fieldname == undefined || fieldname == null) {
    if (this instanceof Object) {
      const result = []
      const map = new Map()
      for (const item of this) {
        if (!map.has(JSON.stringify(item))) {
          result.push(JSON.parse(JSON.stringify(item)))
          map.set(JSON.stringify(item), true)
        }
      }
      return result
    } else {
      return [...new Set(this)]
    }
  } else {
    return [...new Set(this.map(p => p[fieldname]))]
  }
}

Array.prototype.sum = function (fieldname) {
  if (fieldname instanceof Function) {
    return this.reduce((a, b) => a + fieldname(b), 0)
  } else {
    return this.map(p => p[fieldname]).reduce((a,b) => a + b, 0)
  }
  // return this.map(p => p[fieldname]).reduce((a,b) => a + b, 0)
}

String.prototype.contains = function (str) {
  return (this.indexOf(str) !== -1);
};
Number.prototype.pad = function (size) {
  var s = String(this);
  while (s.length < (size || 2)) { s = "0" + s; }
  return s;
}

String.prototype.replaceAll = function (token, newToken, ignoreCase) {
  var str, i = -1, _token;
  if ((str = this.toString()) && typeof token === "string") {
      _token = ignoreCase === true ? token.toLowerCase() : undefined;
      while ((i = (
          _token !== undefined ?
              str.toLowerCase().indexOf(
                  _token,
                  i >= 0 ? i + newToken.length : 0
              ) : str.indexOf(
                  token,
                  i >= 0 ? i + newToken.length : 0
              )
      )) !== -1) {
          str = str.substring(0, i)
              .concat(newToken)
              .concat(str.substring(i + token.length));
      }
  }
  return str;
}

Number.prototype.decimals = function () {
  var num = this
  var num_parts = num.toString().split(".");
  num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return num_parts.join(".");
}

Array.prototype.sortBy = function (fieldname, direction) {
  direction = direction || 'asc'

  return this.sort((a, b) => {
    var aVal = ''
    var bVal = ''

    aVal = a[fieldname]
    bVal = b[fieldname]

    // if (fieldType == 'string') {
    //   aVal = this.onNull(aVal, '')
    //   bVal = this.onNull(bVal, '')
    // } else if (fieldType == 'date') {
    //   aVal = moment(self.onNull(aVal, '01/01/1900'))
    //   bVal = moment(self.onNull(bVal, '01/01/1900'))
    // }

    // console.log(`compare ${aVal} to ${bVal}`)

    if (aVal < bVal) {
      return direction === 'asc' ? -1 : 1
    }
    if (aVal > bVal) {
      return direction === 'asc' ? 1 : -1
    }
    // a must be equal to b
    return 0
  })
}

// if (!String.prototype.repeat) {
  // String.prototype.repeat = function(count) {
  //   'use strict';
  //   if (this == null)
  //     throw new TypeError('can\'t convert ' + this + ' to object');

  //   var str = '' + this;
  //   // To convert string to integer.
  //   count = +count;
  //   // Check NaN
  //   if (count != count)
  //     count = 0;

  //   if (count < 0)
  //     throw new RangeError('repeat count must be non-negative');

  //   if (count == Infinity)
  //     throw new RangeError('repeat count must be less than infinity');

  //   count = Math.floor(count);
  //   if (str.length == 0 || count == 0)
  //     return '';

  //   // Ensuring count is a 31-bit integer allows us to heavily optimize the
  //   // main part. But anyway, most current (August 2014) browsers can't handle
  //   // strings 1 << 28 chars or longer, so:
  //   if (str.length * count >= 1 << 28)
  //     throw new RangeError('repeat count must not overflow maximum string size');

  //   var maxCount = str.length * count;
  //   count = Math.floor(Math.log(count) / Math.log(2));
  //   while (count) {
  //      str += str;
  //      count--;
  //   }
  //   str += str.substring(0, maxCount - str.length);
  //   return str;
  // }
// }
