/* eslint-disable */
import Vue from 'vue'

Vue.directive('focus', {
  inserted: function (el) {
    el.focus()
  }
})

function SetupQTip (el, binding) {
  var text = typeof binding.value === 'object' && binding.value !== null ? binding.value.text : binding.value
  var title = typeof binding.value === 'object' && binding.value !== null ? binding.value.title : ''
  var qtipMyPosition = typeof binding.value === 'object' && binding.value !== null ? binding.value.myPosition || 'left bottom' : 'left bottom'
  var qtipAtPosition = typeof binding.value === 'object' && binding.value !== null ? binding.value.atPosition || 'right top' : 'right top'
  var classes = typeof binding.value === 'object' && binding.value !== null ? binding.value.classes || '' : ''
  var target = typeof binding.value === 'object' && binding.value !== null ? binding.value.target || false : false
  // console.log(`text: ${text}`)
  // console.log(`title: ${title}`)
  // console.log(`qtipMyPosition: ${qtipMyPosition}`)
  // console.log(`qtipAtPosition: ${qtipAtPosition}`)
  // console.log(`classes: ${classes}`)
  // console.log(`target: ${target}`)
  if ($(el).qtip('api') === undefined) {
    if (text !== undefined && text !== null && text.length > 0) {
      $(el).qtip({
        content: {
          title: title,
          text: `<div style="max-height:250px; overflow-y:auto;">${text}</div>`
        },
        hide: {
          fixed: true,
          delay: 500
        },
        position: {
          // adjust: {
          //   viewport: $(window)
          // },
          // target: 'mouse',
          target: target,
          my: qtipMyPosition, // Position my top left...
          at: qtipAtPosition // at the bottom right of...
        },
        show: {
          solo: true
        },
        style: {
          classes: `qtip-shadow ${classes}`
        }
      })
    }
  } else {
    $(el).qtip('api').set('content.text', `<div style="max-height:250px; overflow-y:auto;">${text}</div>`)
    $(el).qtip('api').set('content.title', title)
  }
}

Vue.directive('qtip', {
  inserted: function (el, binding) {
    SetupQTip(el, binding)
  },
  update: function (el, binding) {
    SetupQTip(el, binding)
  }
})
