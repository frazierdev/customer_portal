import Vue from 'vue'
import Vuex from 'vuex'
import * as actions from './actions'
import user from './modules/user'
Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

const store = new Vuex.Store({
  actions,
  modules: {
    user
  },
  strict: debug
})

export default store
