import { EventBus, CHANGE_LOCALE } from '@/EventBus'
import { GetSessionStorage, SetSessionStorage, RemoveSessionStorage, USER_LOCALE, USER_TOKEN, USER_ROLE, USER_PASS, USER_PROXY, FULL_ACCESS, GALLERY_TOKEN, USER_ID } from '../../storage.js'
import { onNull } from '@/functions'
// import { GetSessionStorage, SetSessionStorage, RemoveSessionStorage, USER_LOCALE, USER_TOKEN, USER_ROLE } from '@/Storage'
// import { BaseAPIUrl } from '@/axios-setup'

const state = {
  loggedIn: false,
  userId: GetSessionStorage(USER_ID),
  newPass: GetSessionStorage(USER_PASS),
  token: GetSessionStorage(USER_TOKEN),
  proxyId: GetSessionStorage(USER_PROXY),
  // proxyToken: GetSessionStorage(PROXY_TOKEN),
  locale: GetSessionStorage(USER_LOCALE),
  role: GetSessionStorage(USER_ROLE),
  fullAccess: GetSessionStorage(FULL_ACCESS),
  gallery: GetSessionStorage(GALLERY_TOKEN),
  security: {
  }
}

const getters = {
}

const actions = {
}

const mutations = {
  setLocale(state, payload) {
    state.locale = payload
    EventBus.$emit(CHANGE_LOCALE, payload)
  },
  setUser(state, { token, userRole, userId, proxyId = 0, fullAccess = false }) {
    state.token = token
    state.userId = userId
    state.role = userRole
    state.proxyId = proxyId
    state.fullAccess = fullAccess
    SetSessionStorage(USER_TOKEN, token)
    SetSessionStorage(USER_ROLE, userRole)
    SetSessionStorage(USER_ID, userId)
    SetSessionStorage(USER_PROXY, proxyId)
    SetSessionStorage(FULL_ACCESS, fullAccess)
    state.loggedIn = true
    window.dispatchEvent(new CustomEvent('update-proxy-info'))
    // if (process.env.NODE_ENV !== 'production') {
    //   console.log(`state.token: ${state.token}`)
    //   console.log(`state.role: ${state.role}`)
    //   console.log(`state.proxy: ${state.proxyId}`)
    // }
  },
  updateUser(state, { token, userRole, proxyId, fullAccess }) {

    if (onNull(token, undefined) !== undefined) {
      // console.warn(`token: ${token}`)
      state.token = token
      SetSessionStorage(USER_TOKEN, token)
    }

    if (onNull(userRole, undefined) !== undefined) {
      // console.warn(`userRole: ${userRole}`)
      state.role = userRole
      SetSessionStorage(USER_ROLE, userRole)
    }

    if (onNull(proxyId, undefined) !== undefined) {
      // console.warn(`proxyId: ${proxyId}`)
      if (proxyId === 'undefined') {
        proxyId = '0'
      }
      state.proxyId = proxyId
      SetSessionStorage(USER_PROXY, proxyId)
    }

    if (onNull(fullAccess, undefined) !== undefined) {
      // console.warn(`fullAccess: ${fullAccess}`)
      state.fullAccess = fullAccess
      SetSessionStorage(FULL_ACCESS, fullAccess)
    }
  },
  removeUser(state) {
    state.token = null
    state.userId = null
    state.role = null
    state.proxyToken = null
    state.proxyId = '0'
    state.fullAccess = false
    RemoveSessionStorage(USER_TOKEN)
    RemoveSessionStorage(USER_ID)
    RemoveSessionStorage(USER_ROLE)
    RemoveSessionStorage(USER_PROXY)
    RemoveSessionStorage(FULL_ACCESS)
    state.loggedIn = false
  },
  requirePass(state, required) {
    state.newPass = required
    // console.log(`state.newPass: ${state.newPass}`)
    if (required) {
      SetSessionStorage(USER_PASS, required)
    } else {
      RemoveSessionStorage(USER_PASS)
    }
  },
  setGalleryAccess(state, token) {
    state.gallery = token

    if (token) {
      SetSessionStorage(GALLERY_TOKEN, token)
    } else {
      RemoveSessionStorage(GALLERY_TOKEN)
    }
  }
  // setProxy (state, { token, userRole }) {
  //   state.token = token
  //   state.role = userRole
  //   SetSessionStorage(USER_TOKEN, token)
  //   SetSessionStorage(USER_ROLE, userRole)
  //   state.loggedIn = true
  // },
  // removeProxy (state) {
  //   state.token = null
  //   state.role = null
  //   state.proxyToken = null
  //   RemoveSessionStorage(USER_TOKEN)
  //   RemoveSessionStorage(USER_ROLE)
  //   // RemoveSessionStorage(PROXY_TOKEN)
  //   state.loggedIn = false
  // }
  // setProxy (state, { token }) {
  //   // state.proxyToken = proxyToken
  //   // SetSessionStorage(PROXY_TOKEN, proxyToken)
  // },
  // removeProxy (state) {
  //   state.proxyToken = null
  //   RemoveSessionStorage(PROXY_TOKEN)
  // }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
