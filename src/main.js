import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'
import Snotify from 'vue-snotify'
import VModal from 'vue-js-modal'
import VueSwal from 'vue-swal'
import store from '@/store'
import 'babel-polyfill'

require('./plugins/axios')
require('./prototypes-additions')
require('./vue-directives')
require('es6-promise').polyfill()
require('./vue-mixins')
require('./vue-components')

Vue.config.productionTip = false

const options = {
  toast: {
    oneAtTime: true,
    preventDuplicates: true
  }
}

Vue.component('scheduler', {
  props: ['id', 'config'],
  mounted: function() {
    this.control = new DayPilot.Scheduler(this.id, this.config).init() // eslint-disable-line
  },
  template: '<div :id="id"></div>'
})

Vue.use(VModal)
Vue.use(VueSwal)
Vue.use(Snotify, options)

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
