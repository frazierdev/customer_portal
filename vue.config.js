module.exports = {
  transpileDependencies: ['vuetify'],

  pluginOptions: {
    moment: {
      locales: [
        ''
      ]
    }
  },

  publicPath: './',
  // baseUrl: './',

  chainWebpack: config => {
  // remove the prefetch plugin
    config.plugins.delete('prefetch')
  },

  runtimeCompiler: true
}
