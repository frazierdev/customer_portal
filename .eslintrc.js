module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ['plugin:vue/essential', 'eslint:recommended'],
  rules: {
    'no-console': 'off',
    'no-debugger': 'off',
    'vue/no-unused-components': 'warn',
    'vue/no-unused-vars': 'warn',
    'comma-spacing': 'error',
    'no-trailing-spaces': 'error',
    'space-before-function-paren': ['warn', 'never'],
    'space-in-parens': ['warn', 'never'],
    'func-call-spacing': ['warn', 'never'],
    'arrow-spacing': 'warn',
    // 'object-curly-newline': ['error', { 'consistent': true }],
    // 'object-curly-newline': ['error', { 'multiline': true, 'minProperties': 3 }],
    'object-curly-spacing': ['error', 'always'],
    quotes: ['error', 'single'],
    'no-unused-vars': 'warn',
    'no-undef': 'warn',
    'no-empty': 'warn',
    'no-extra-semi': 'warn',
    'vue/no-side-effects-in-computed-properties': 'warn',
    'vue/return-in-computed-property': 'warn',
    'no-inner-declarations': 'warn',
    'vue/no-async-in-computed-properties': 'warn',
    'no-redeclare': 'warn',
    'no-prototype-builtins': 'warn',
    'vue/require-valid-default-prop': 'warn',
    'vue/valid-template-root': 'warn',
    'vue/html-closing-bracket-newline': ['warn', { multiline: 'never' }],
    'vue/order-in-components': 'warn',
    'vue/valid-v-slot': ['warn', { allowModifiers: true }],
    'vue/multi-word-component-names': 'warn', // 'off'
    'no-extra-parens': ['warn', 'all']
  },
  parserOptions: {
    // parser: 'babel-eslint'
    parser: '@babel/eslint-parser',
    // requireConfigFile: false
  }
};
